\documentclass[11pt,letterpaper]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{pstricks}
\usepackage{booktabs}
% Add some padding between caption and table toprule
\setlength{\abovetopsep}{2ex}
% Add some vertical padding between table elements
\renewcommand{\arraystretch}{1.2}
%\usepackage{xcolor}
%\definecolor{darkblue}{rgb}{0,0,0.5} 
%\usepackage{transparent}
%\usepackage[adobe-utopia]{mathdesign}
\usepackage[left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}
\author{Wesley Henderson}
\title{Intermodal Network Design Progress Report}
\begin{document}
\maketitle
\tableofcontents

\section{Introduction}

This brief report documents the accomplishments made recently toward the intermodal network design project.
The main goal of this project is to develop a method for designing intermodal transportation networks using an inference-based approach.
Network design in this context involves choosing locations for new terminals with the ultimate goal of increasing economic competitiveness, reducing environmental impact, and limiting the total network operating cost.
Terminal characteristics that affect cost are capacity, location, and the fixed cost of building and operating the terminal.
A terminal with a greater capacity is able to move more traffic from the highway mode to the less-costly rail mode.
However, a terminal with a larger capacity will often require a higher fixed cost than a terminal with a smaller capacity.
Finally, a terminal that is close to several nodes simultaneously will generally offer more overall utility to the network.

To determine the utility of a terminal to the network as a whole, freight demand must be routed through the network in such a way that terminal capacity is fully utilized in an efficient manner and freight demand estimates are met.
We accomplish this routing through the use of Bayesian parameter estimation and nested sampling. 
A particular terminal scheme has an evidence value that is estimated concurrently with the optimal routing scheme.
We should be able to use the evidence value to objectively compare proposed terminal additions.

\section{Likelihood and Evidence}
In the past, how well a particular routing scheme meets demand estimates was included in the likelihood.
The current implementation enforces that requirement in the prior.

The likelihood for a routing scheme depends on the cost energy $Q_C^2$ and the capacity energy $Q_T^2$ of that scheme:
\begin{equation}
\log L = -\frac{1}{\sigma_C^2} Q_C^2 - \frac{1}{\sigma_T^2} Q_T^2 \mathrm{.}
\end{equation}
These quantities measure the overall cost of the scheme and how badly the scheme exceeds capacity limits, respectively. The cost energy depends on the per-unit cost of each route $m$ that satisfies demand $n$, $c_n^m$, the amount of demand traversing those routes, $x_n^m$, and the fixed cost of each terminal $k$, $F_k$:
\begin{equation}
Q_C^2 = \sum_{n=1}^N \sum_{m=0}^M c_n^m x_n^m + \sum_{k=1}^K F_k \mathrm{.}
\end{equation}
The capacity energy for a terminal is zero if the demand flowing through the terminal does not exceed its capacity, and non-zero otherwise:
\begin{equation}
Q_T^2 = \sum_{k=1}^K \Delta T_k^2 \mathrm{,} ~~\mathrm{with}~ \Delta T_k^2 = \left\{
	\begin{array}{lr}
		t_k - T_k & : t_k > T_k\\
		0 & : t_k \leq T_k
	\end{array}
\right. \mathrm{.}
\end{equation}

Nested sampling estimates the evidence by numerically evaluating the following integral:
\begin{equation}
Z = \int_0^1 L(\xi)\, \mathrm{d}\xi \mathrm{,}
\end{equation}
where $\xi$ is the portion of the prior mass contained within likelihood constraint $L$. 
Therefore, our assignment of the likelihood function will directly influence the value of the evidence.

\section{MCMC}
When we last met, I was still having some trouble getting reasonable and consistent results using my nested sampling implementation. 
At that point, I was using Metropolis sampling to draw new samples. 
Since then, I've seen much better results using a variation on Gibbs sampling to replace killed samples.
In simple test cases, demand is routed consistently along the least-cost routes, and capacity constraints are respected.

\section{Test Scenarios}
To explore the relationship between fixed cost, capacity, and log-evidence, I examined eight different scenarios.
In each of these scenarios, the cost standard deviation ($\sigma_C$) and the capacity standard deviation ($\sigma_T$) were both set to $0.1$.
The node and terminal locations are as shown in Figure \ref{fig:locations}. 
Each scenario varied the fixed cost and/or capacity of the bottom-left terminal. 
The details for the scenarios are shown in Table \ref{tab:scenarios}. In these examples, 500 tons of freight are moving from each node to each other node, and rail costs are 10\% of highway costs.

\begin{figure}
\begin{center}
\def\svgwidth{\columnwidth}
%\input{triangle_layout_I.tex}
\includegraphics[width=3in]{nodes_terminals_with_distances.png}
\caption{Node and terminal locations. Nodes are circles and terminals are rectangles.}
\label{fig:locations}
\end{center}
\end{figure}

\begin{table}
\begin{center}
\caption{List of scenario details and results. Listed fixed cost and capacity are for terminal 2. The ``cost'' value does not include fixed cost.}
\label{tab:scenarios}

\begin{tabular}{@{}ccccc@{}}
\toprule
Fixed cost (\$) & Capacity (tons) & logZ & Cost (\$) & Demand processed (tons)\\ 
\midrule
0 & 2000 & $-3.7865 \times 10^6$ & 37848 & 2000\\  
300 & 1000 & $-6.6880 \times 10^6$ & 66333 & 1015\\ 
300 & 500 & $-8.1457 \times 10^6$ & 80838 & 515.8\\ 
300 & 2000 & $-3.9808 \times 10^6$ & 39316 & 1997\\ 
100 & 1000 & $-6.6650 \times 10^6$ & 66268 & 1016\\ 
20000 & 1000 & $-8.6557 \times 10^6$ & 86107 & 1014\\  
20000 & 2000 & $-6.1043 \times 10^6$ & 40790 & 1922\\ 
10000 & 500 & $-9.1348 \times 10^6$ & 81075 & 515.5\\ 
\bottomrule
\end{tabular} 

\end{center}
\end{table}

I also tried setting the fixed cost of each terminal to 0 and the capacity for each terminal to a very large value, then varying the location and number of the terminals. 
For this test, I looked at five scenarios, detailed in Figure \ref{fig:five_configs}. 
The log-evidence and cost estimated for each configuration is shown in Table \ref{tab:five_configs_results}.

\begin{figure}
\begin{center}
\includegraphics[width=5in]{five_configs_2}
\caption{Five terminal configurations}
\label{fig:five_configs}
\end{center}
\end{figure}

\begin{table}
\begin{center}
\caption{Log-evidence and cost for each terminal configuration}
\label{tab:five_configs_results}

\begin{tabular}{@{}cccccccc@{}}
\toprule
\multicolumn{3}{c}{~} & \phantom{abc} & \multicolumn{4}{c}{Demand processed (tons)} \\
\cmidrule{5-8}
Configuration & logZ & cost (\$) && 1 & 2 & 3 & 4 \\ 
\midrule
I & $-3.7865 \times 10^6$ & 37848 && 2000 & 2000 & 2000 & - \\ 
II & $-6.8718 \times 10^6$ & 68592 && 1859 & 1845 & 2014 & - \\ 
III & $-3.2769 \times 10^6$ & 32413 && 1987 & 1989 & 1999 & - \\ 
IV & $-4.5642 \times 10^6$ & 45487 && 1498 & 1972 & 1997 & 473 \\ 
V & $-9.4685 \times 10^6$ & 94683 && 2000 & 2000 & - & - \\ 
\bottomrule
\end{tabular} 

\end{center}
\end{table}

\section{Discussion}
The results shown in Table \ref{tab:scenarios} show that fixed cost and capacity affect evidence in an expected fashion: significant increases in fixed terminal cost push the evidence down, while increases in capacity push the evidence up. 
The results in Table \ref{tab:five_configs_results} show that terminal location and count also affects evidence in the way we expected. 
Terminals in awkward locations, such as in configuration II, drive the evidence down, while terminals in more accessible locations, such as in configuration III, raise the evidence. 
Adding unnecessary terminals (configuration IV) slightly lowers the evidence, and removing obviously useful terminals (configuration V) drops the evidence considerably.

These observations suggest that the evidence will serve as a useful decision criterion for comparing terminal design schemes.

\end{document}